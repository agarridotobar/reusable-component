import './App.css';

function App() {
  return (
    <section className="layout">
      <header>
			  Header
		  </header>
      <nav>
        Navigation
        <ul>
          <li>
            link 1
          </li>
          <li>
            link 2
          </li>
          <li>
            link 3
          </li>
        </ul>
      </nav>
	  	<main>
			  Content
        <br />
        <br />
        <p>Lorem ipsum dolor sit amet consectetur adipiscing 
          elit nulla, elementum potenti accumsan maecenas himenaeos tincidunt erat, primis cras natoque feugiat urna ante cum. Nascetur tincidunt eget praesent donec ullamcorper proin, dictum ad semper quisque magna, gravida aenean lobortis pharetra mollis. Convallis eget ad posuere eu cubilia lectus, enim nam lacus conubia duis, natoque viverra sodales sapien mauris.
          Congue aenean massa scelerisque sollicitudin molestie 
          pellentesque class erat potenti, pulvinar cras nisl 
          iaculis commodo felis cursus eu venenatis, leo duis 
          praesent lacinia lacus in volutpat torquent. Senectus 
          nullam semper platea euismod class congue etiam feugiat natoque fringilla aliquam, mus sodales sem aliquet ultricies tellus justo hendrerit est. Curae convallis luctus lectus rhoncus nullam purus praesent habitant, cubilia arcu tortor laoreet nisl libero erat penatibus nisi, hac vitae per scelerisque porta tempor cum.</p>
          <br />
          <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
		  </main>      
      <footer>
        Footer
      </footer>
    </section>
  );
}

export default App;
