import './bitbucket.css';
import './fonts.css';

import { Titlebuttons, Breadcrumb } from './components';

function Bitbucket() {

  const copyRoutes = [
    { path: "23people" }, 
    { path: "Grasshopper" }
  ];

  return (
    <section className="layout">
      <header>Header</header>

      <nav>Navigation</nav>

      <nav className="nav-responsive">
        <span className="icon-menu"></span>
          Navigation 2
        <span className="icon-plus"></span>
      </nav>

      <main>
        <Breadcrumb routes={copyRoutes} />
        <Titlebuttons />
        <p>Here's where you'll find this repository's source files. To give your users an idea of what they'll find here, <a href="#">add a description to your repository.</a></p>
          <br />
          <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
      </main>
      
      <aside>Asaide</aside>
    </section>
  );
}

export default Bitbucket;