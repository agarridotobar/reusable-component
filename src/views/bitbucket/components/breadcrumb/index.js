import './breadcrumb.css';
import { BrowserRouter as Router, Link } from 'react-router-dom';


function Breadcrumb(routes) {
  const copy = routes.routes;

  return (
    <Router>
      <section className="content-Breadcrumb">
        <p className="routes">
          {copy.map( route => (<Link className="routes" to={route.path}> / {route.path}</Link>))}
        </p>
      </section>
    </Router>
  );
};

export default Breadcrumb;
