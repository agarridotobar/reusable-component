import './button.css';

function Button({title="Button", onClick, icon}) {
  return (
    <section className="content-Button">
      <button type="button" onClick={() => onClick()}>
        { icon ? <span className={icon}></span> : null } 
        {title}
      </button>
    </section>
  )
}

export default Button;
