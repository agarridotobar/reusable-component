import './title.css';

function Title({ title }) {
  return (
    <section className="content-Title">
      {title}
    </section>
  );
}

export default Title;
