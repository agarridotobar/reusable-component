import './titlebuttons.css';

import { Button, Title } from '../index';

const Titlebuttons = () => {
  const handleClone = () => {
    console.log('Press button Clone');
  };

  const handleMore = () => {
    console.log('Press button More');
  };

  const title = "grasshopper-frontend";


  return (
    <section className="content-Titlebuttons">
      <Title title={title}/>
      <div className="content-buttons">
        <Button title="Clone" onClick={handleClone} />
        <Button title="" onClick={handleMore} icon="icon-flickr" />  
      </div>
    </section>
  )
};

export default Titlebuttons;
